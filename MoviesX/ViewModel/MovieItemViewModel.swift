//
//  MovieItemViewModel.swift
//  MoviesX
//
//  Created by Mufakkharul Islam Nayem on 11/5/22.
//

import UIKit

struct MovieItemViewModel {
    let id: Int
    let poster: String?
    let title: String
    let releaseDate: String?
    let voteAverage: String
    let voteCount: String
    let genreNames: [String]
    
    let select: () -> Void
    let dateFormattingClosure: CustomDateFormatter?
    
    var posterURL: URL? {
        guard let posterPath = poster else { return nil }
        return URL(string: posterPath)
    }
    
    var voteAverageFormatted: NSAttributedString {
        let imageAttachment = NSTextAttachment()
        imageAttachment.image = UIImage(systemName: "star.fill")?
            .withTintColor(.systemBlue)

        let fullString = NSMutableAttributedString(attachment: imageAttachment)
        fullString.append(NSAttributedString(string: " \(voteAverage)"))
        return fullString
    }
    
    var voteCountFormatted: String {
        return "out of \(voteCount) votes"
    }
    
    var releaseDateFormatted: String? {
        guard let releaseDate = releaseDate else { return nil }
        return dateFormattingClosure?(releaseDate)
    }
}

extension MovieItemViewModel: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    static func == (lhs: MovieItemViewModel, rhs: MovieItemViewModel) -> Bool {
        return lhs.id == rhs.id
    }
}

extension MovieItemViewModel {
    init(movie: Movie, genres: [Genre], selection: @escaping () -> Void, dateFormattingClosure: CustomDateFormatter?) {
        id = movie.id
        poster = movie.fullPosterPath
        title = movie.title
        releaseDate = movie.releaseDate
        voteAverage = String(movie.voteAverage)
        voteCount = String(movie.voteCount)
        select = selection
        
        let matchingGenres = genres.filter { movie.genreIDS.contains($0.id) }
        genreNames = matchingGenres.map { $0.name }
        self.dateFormattingClosure = dateFormattingClosure
    }
}
