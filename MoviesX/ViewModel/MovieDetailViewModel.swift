//
//  MovieDetailViewModel.swift
//  MoviesX
//
//  Created by Mufakkharul Islam Nayem on 15/5/22.
//

import UIKit

struct MovieDetailViewModel {
    
    typealias NewMovieDetailClosure = (Result<Self, Error>) -> Void
    typealias FavoriteActionClosure = (@escaping NewMovieDetailClosure) -> Void
    
    let backdrop: String?
    let poster: String?
    let title: String
    let tagline: String
    let genreTags: [String]
    let overview: String
    let releaseDate: String?
    let voteAverage: String
    let voteCount: String
    let language: String
    let status: String
    
    let favorite: Bool
    let addToFavorite: FavoriteActionClosure
    let removeFromFavorite: FavoriteActionClosure
    
    let dateFormattingClosure: CustomDateFormatter?
    
    var backdropURL: URL? {
        guard let backdropPath = backdrop else { return nil }
        return URL(string: backdropPath)
    }
    
    var posterURL: URL? {
        guard let posterPath = poster else { return nil }
        return URL(string: posterPath)
    }
    
    var voteAverageFormatted: NSAttributedString {
        let imageAttachment = NSTextAttachment()
        imageAttachment.image = UIImage(systemName: "star.fill")?
            .withTintColor(.systemBlue)

        let fullString = NSMutableAttributedString(attachment: imageAttachment)
        fullString.append(NSAttributedString(string: " \(voteAverage)"))
        return fullString
    }
    
    var voteCountFormatted: String {
        return "\(voteCount) votes"
    }
    
    var releaseDateFormatted: String? {
        guard let releaseDate = releaseDate else { return nil }
        return dateFormattingClosure?(releaseDate)
    }
    
    var favoriteButtonText: String {
        if favorite {
            return "Remove from Favorites"
        } else {
            return "Add to Favorites"
        }
    }
    
    var favoriteButtonColor: UIColor {
        if favorite {
            return .systemRed
        } else {
            return .systemOrange
        }
    }
    
    func handleFavoriteButtonAction(completion: @escaping NewMovieDetailClosure) {
        if favorite {
            removeFromFavorite(completion)
        } else {
            addToFavorite(completion)
        }
    }
}

extension MovieDetailViewModel {
    init(movieDetails: MovieDetails,
         isFavorite: Bool,
         addToFavorite: @escaping FavoriteActionClosure,
         removeFromFavorite: @escaping FavoriteActionClosure,
         dateFormattingClosure: CustomDateFormatter?) {
        backdrop = movieDetails.fullBackdropPath
        poster = movieDetails.fullPosterPath
        title = movieDetails.originalTitle
        tagline = movieDetails.tagline
        genreTags = movieDetails.genres.map({ $0.name })
        overview = movieDetails.overview
        releaseDate = movieDetails.releaseDate
        voteAverage = String(movieDetails.voteAverage)
        voteCount = String(movieDetails.voteCount)
        language = movieDetails.spokenLanguages.map( { $0.name }).joined(separator: ", ")
        status = movieDetails.status
        favorite = isFavorite
        self.addToFavorite = addToFavorite
        self.removeFromFavorite = removeFromFavorite
        self.dateFormattingClosure = dateFormattingClosure
    }
}
