//
//  MovieDetailsContainerViewController.swift
//  MoviesX
//
//  Created by Mufakkharul Islam Nayem on 15/5/22.
//

import UIKit

class MovieDetailsContainerViewController: UIViewController {
    
    @IBOutlet private weak var containerView: UIView!
    
    var service: MovieDetailsItemService?
    
    private var movieDetailsViewController: MovieDetailsViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadDetailsData()
    }
    
    private func loadDetailsData() {
        startLoadingAnimation()
        containerView.isHidden = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination
        if let movieDetails = destination as? MovieDetailsViewController {
            movieDetailsViewController = movieDetails
            movieDetails.service = service
            
            movieDetails.callbacks.didSucceedLoadingDetails = { [weak self] in
                self?.stopLoadingAnimation()
                self?.containerView.isHidden = false
            }
            
            movieDetails.callbacks.didFailLoadingDetails = { [weak self] error in
                self?.stopLoadingAnimation()
                // FIXME: show a error state and keep the container view hidden
            }
            
        }
    }
    
}

extension MovieDetailsContainerViewController: StoryboardInstantiable {
    static var storyboardName: UIStoryboard.Name {
        .main
    }
}
