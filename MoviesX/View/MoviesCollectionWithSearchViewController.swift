//
//  MoviesCollectionWithSearchViewController.swift
//  MoviesX
//
//  Created by Mufakkharul Islam Nayem on 16/5/22.
//

import UIKit

class MoviesCollectionWithSearchViewController: MoviesCollectionViewController {
    
    class Callbacks {
        var didTapFavorites: () -> Void = { }
    }
    
    let callbacks = Callbacks()
    var searchService: MovieSearchAPIItemServiceAdapter?
    
    private lazy var moviesSearchResultsVC: MoviesCollectionViewController = {
        let vc = MoviesCollectionViewController(collectionViewLayout: UICollectionViewFlowLayout())
        return vc
    }()
    
    private lazy var searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: moviesSearchResultsVC)
        searchController.searchBar.delegate = self
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = true
        searchController.searchBar.placeholder = "Search Movies"
        definesPresentationContext = true
        return searchController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.searchController = searchController
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: UIImage(systemName: "heart.fill")?
                .withTintColor(.systemOrange)
                .withRenderingMode(.alwaysOriginal),
            style: .done,
            target: self,
            action: #selector(favoritesButtonTapped(_:)))
    }
    
    @objc private func favoritesButtonTapped(_ sender: UIBarButtonItem) {
        callbacks.didTapFavorites()
    }
    
}

extension MoviesCollectionWithSearchViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let searchText = searchBar.text, !searchText.isEmpty else { return }
        
        searchService?.query = searchText
        moviesSearchResultsVC.service = searchService
        moviesSearchResultsVC.loadMovieItems()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        moviesSearchResultsVC.service = nil
    }
}

extension MoviesCollectionWithSearchViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if searchController.searchBar.searchTextField.isFirstResponder {
            searchController.showsSearchResultsController = true
        }
    }
}
