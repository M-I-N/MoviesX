//
//  MovieDetailsViewController.swift
//  MoviesX
//
//  Created by Mufakkharul Islam Nayem on 15/5/22.
//

import UIKit
import Kingfisher
import TagListView

protocol MovieDetailsItemService {
    func loadMovieDetails(completion: @escaping (Result<MovieDetailViewModel, Error>) -> Void)
}

class MovieDetailsViewController: UITableViewController {
    
    @IBOutlet private weak var backdropImageView: UIImageView!
    @IBOutlet private weak var posterImageView: UIImageView!
    @IBOutlet private weak var originalTitleLabel: UILabel!
    @IBOutlet private weak var taglineLabel: UILabel!
    @IBOutlet private weak var overviewLabel: UILabel!
    @IBOutlet private weak var voteAverageLabel: UILabel!
    @IBOutlet private weak var voteCountLabel: UILabel!
    @IBOutlet private weak var releaseDateLabel: UILabel!
    @IBOutlet private weak var statusLabel: UILabel!
    @IBOutlet private weak var languageLabel: UILabel!
    @IBOutlet private weak var favoriteButton: UIButton!
    @IBOutlet private weak var genreTagListView: TagListView!
    
    var service: MovieDetailsItemService?
    private var movieDetails: MovieDetailViewModel?
    
    let callbacks = Callbacks()
    
    class Callbacks {
        var didSucceedLoadingDetails: () -> Void = { }
        var didFailLoadingDetails: (Error) -> Void = { _ in }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadMovieDetails()
    }
    
    private func loadMovieDetails() {
        service?.loadMovieDetails { [weak self] result in
            switch result {
            case .success(let detail):
                self?.movieDetails = detail
                self?.refreshDataBinding()
                self?.callbacks.didSucceedLoadingDetails()
            case .failure(let error):
                print(error)
                self?.callbacks.didFailLoadingDetails(error)
            }
        }
    }
    
    private func refreshDataBinding() {
        guard let details = movieDetails else { return }
        backdropImageView.kf.setImage(with: details.backdropURL)
        posterImageView.kf.setImage(with: details.posterURL)
        originalTitleLabel.text = details.title
        taglineLabel.text = details.tagline
        genreTagListView.removeAllTags()
        genreTagListView.addTags(details.genreTags)
        overviewLabel.text = details.overview
        voteAverageLabel.attributedText = details.voteAverageFormatted
        voteCountLabel.text = details.voteCountFormatted
        releaseDateLabel.text = details.releaseDateFormatted
        statusLabel.text = details.status
        languageLabel.text = details.language
        
        favoriteButton.setTitle(details.favoriteButtonText, for: .normal)
        favoriteButton.backgroundColor = details.favoriteButtonColor
        
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    @IBAction private func favoriteButtonTapped(_ sender: UIButton) {
        // FIXME: Show a loader maybe?
        sender.isEnabled = false
        movieDetails?.handleFavoriteButtonAction { [weak self] result in
            sender.isEnabled = true
            switch result {
            case .success(let updatedMovieDetails):
                self?.movieDetails = updatedMovieDetails
                self?.refreshDataBinding()
            case .failure(let error):
                print(error)
                // FIXME: Show an error maybe?
            }
        }
    }
    
}

extension MovieDetailsViewController {
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
