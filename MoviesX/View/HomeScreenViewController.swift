//
//  HomeScreenViewController.swift
//  MoviesX
//
//  Created by Mufakkharul Islam Nayem on 11/5/22.
//

import UIKit

/// The entry screen of the App.
///
/// This acts as the root of composition layer.
class HomeScreenViewController: UITabBarController {
    
    // In a typical app, these stores are created once and passed around as dependency
    // As we are in the composition root, creating them here
    private let favoritesStore = FavoritesStore.shared
    private let genreStore = GenreStore.shared
    
    private lazy var genreService: GenreAPIServiceAdapter = {
        let service = GenreAPIServiceAdapter(api: .shared, store: genreStore)
        return service
    }()

    // inject dependency through initializer if needed
    convenience init() {
        self.init(nibName: nil, bundle: nil)
        self.setupViewController()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        genreService.preheatGenreList()
    }

    private func setupViewController() {
        viewControllers = [
            makeNav(for: makeNowPlayingList(), icon: "play.circle.fill"),
            makeNav(for: makePopularList(), icon: "crown.fill"),
            makeNav(for: makeTopRatedList(), icon: "chevron.up.circle.fill"),
            makeNav(for: makeUpcomingList(), icon: "calendar.circle.fill")
        ]
    }
    
    private func makeNav(for vc: UIViewController, icon: String) -> UIViewController {
        vc.navigationItem.largeTitleDisplayMode = .always
        
        let nav = UINavigationController(rootViewController: vc)
        nav.tabBarItem.image = UIImage(systemName: icon,
                                       withConfiguration: UIImage.SymbolConfiguration(scale: .large))
        nav.tabBarItem.title = vc.title
        nav.navigationBar.prefersLargeTitles = true
        return nav
    }
    
    private func makeNowPlayingList() -> MoviesCollectionViewController {
        let vc = MoviesCollectionWithSearchViewController(collectionViewLayout: UICollectionViewFlowLayout())
        vc.title = "Now Playing"
        
        let service = MoviesAPIItemServiceAdapter(
            api: .shared,
            genreStore: genreStore,
            movieType: .nowPlaying,
            selection: { [vc, favoritesStore] movie in
                vc.showDetails(of: movie, store: favoritesStore)
            },
            dateFormattingClosure: DateFormatter.convertedDateString(from:)
        )
        
        let searchService = MovieSearchAPIItemServiceAdapter(
            api: .shared,
            genreStore: genreStore, selection: { [vc, favoritesStore] movie in
                vc.showDetails(of: movie, store: favoritesStore)
            }, dateFormattingClosure: DateFormatter.convertedDateString(from:))
        
        vc.service = service
        vc.searchService = searchService
        
        vc.callbacks.didTapFavorites = { [weak self, vc] in
            self?.showFavoritesViewController(from: vc)
        }
        
        return vc
    }
    
    private func makePopularList() -> MoviesCollectionViewController {
        let vc = MoviesCollectionViewController(collectionViewLayout: UICollectionViewFlowLayout())
        vc.title = "Popular"
        
        let service = MoviesAPIItemServiceAdapter(
            api: .shared,
            genreStore: genreStore,
            movieType: .popular,
            selection: { [vc, favoritesStore] movie in
                vc.showDetails(of: movie, store: favoritesStore)
            },
            dateFormattingClosure: DateFormatter.convertedDateString(from:)
        )
        
        vc.service = service
        return vc
    }
    
    private func makeTopRatedList() -> MoviesCollectionViewController {
        let vc = MoviesCollectionViewController(collectionViewLayout: UICollectionViewFlowLayout())
        vc.title = "Top Rated"
        
        let service = MoviesAPIItemServiceAdapter(
            api: .shared,
            genreStore: genreStore,
            movieType: .topRated,
            selection: { [vc, favoritesStore] movie in
                vc.showDetails(of: movie, store: favoritesStore)
            },
            dateFormattingClosure: DateFormatter.convertedDateString(from:)
        )
        
        vc.service = service
        return vc
    }
    
    private func makeUpcomingList() -> MoviesCollectionViewController {
        let vc = MoviesCollectionViewController(collectionViewLayout: UICollectionViewFlowLayout())
        vc.title = "Upcoming"
        
        let service = MoviesAPIItemServiceAdapter(
            api: .shared,
            genreStore: genreStore,
            movieType: .upcoming,
            selection: { [vc, favoritesStore] movie in
                vc.showDetails(of: movie, store: favoritesStore)
            },
            dateFormattingClosure: DateFormatter.convertedDateString(from:)
        )
        
        vc.service = service
        return vc
    }
    
    private func showFavoritesViewController(from viewController: UIViewController) {
        let favoritesVC = MoviesCollectionViewController(collectionViewLayout: UICollectionViewFlowLayout())
        favoritesVC.title = "My Favorites"
        favoritesVC.hidesBottomBarWhenPushed = true
        
        let service = FavoritesStoreItemServiceAdapter(
            store: favoritesStore,
            genreStore: genreStore,
            select: { [favoritesVC, favoritesStore] movie in
                favoritesVC.showDetails(of: movie, store: favoritesStore)
            },
            dateFormattingClosure: DateFormatter.convertedDateString(from:)
        )
        
        favoritesVC.service = service
        
        viewController.show(favoritesVC, sender: viewController)
    }
}

extension UIViewController {
    func showDetails(of movie: Movie, store: FavoritesStore) {
        let details = MovieDetailsContainerViewController.instantiateFromStoryboard()
        details.title = movie.title
        
        let service = MovieDetailsAPIItemServiceAdapter(
            movie: movie,
            api: .shared,
            store: store,
            dateFormattingClosure: DateFormatter.convertedDateString(from:)
        )
        
        details.service = service
        show(details, sender: self)
    }
}
