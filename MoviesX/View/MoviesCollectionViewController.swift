//
//  MoviesCollectionViewController.swift
//  MoviesX
//
//  Created by Mufakkharul Islam Nayem on 11/5/22.
//

import UIKit

protocol ItemService {
    func loadFirstPageItems(completion: @escaping (Result<[MovieItemViewModel], Error>) -> Void)
    func loadNextPageItems(completion: @escaping (Result<[MovieItemViewModel], Error>) -> Void)
}

class MoviesCollectionViewController: UICollectionViewController {
    
    enum Section {
        case main
    }
    
    enum LoadState {
        case loading
        case idle
    }
    
    typealias DataSource = UICollectionViewDiffableDataSource<Section, MovieItemViewModel>
    typealias Snapshot = NSDiffableDataSourceSnapshot<Section, MovieItemViewModel>
    
    var service: ItemService? {
        didSet {
            items = []
        }
    }
    
    private var loadState = LoadState.idle
    
    private var items = [MovieItemViewModel]() {
        didSet {
            DispatchQueue.mainAsyncIfNeeded {
                self.applySnapshot()
            }
        }
    }
    
    private lazy var dataSource = makeDataSource()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureCollectionViewCompositionalLayout()
        // Register cell classes
        MovieItemCell.registerForReuse(with: collectionView)
        loadMovieItems()
    }
    
    private func makeDataSource() -> DataSource {
        let dataSource = DataSource(collectionView: collectionView) { collectionView, indexPath, item in
            let cell = MovieItemCell.dequeue(fromCollectionView: collectionView, atIndex: indexPath)
            cell.viewModel = item
            return cell
        }
        return dataSource
    }
    
    private func applySnapshot(animatingDifferences: Bool = true) {
        var snapshot = Snapshot()
        snapshot.appendSections([.main])
        snapshot.appendItems(items)
        dataSource.apply(snapshot, animatingDifferences: animatingDifferences)
    }
    
    func loadMovieItems() {
        guard let service = service, loadState == .idle else { return }
        
        loadState = .loading
        startLoadingAnimation()
        service.loadFirstPageItems { [weak self] result in
            self?.loadState = .idle
            self?.stopLoadingAnimation()
            self?.handleResult(result)
        }
    }
    
    private func loadMoreMovieItems() {
        guard loadState == .idle else { return }
        
        loadState = .loading
        service?.loadNextPageItems { [weak self] result in
            self?.loadState = .idle
            self?.handleResult(result)
        }
    }
    
    private func handleResult(_ result: Result<[MovieItemViewModel], Error>) {
        switch result {
        case .success(let items):
            self.items += items
        case .failure(let error):
            print(error)
        }
    }
    
}

extension MoviesCollectionViewController {
    private func configureCollectionViewCompositionalLayout() {
        collectionView.collectionViewLayout = UICollectionViewCompositionalLayout { (_, _) -> NSCollectionLayoutSection? in
            let size = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalWidth(1 / 2))
            let itemCount = 1
            let item = NSCollectionLayoutItem(layoutSize: size)
            let group = NSCollectionLayoutGroup.horizontal(layoutSize: size, subitem: item, count: itemCount)
            group.interItemSpacing = .fixed(10)
            let section = NSCollectionLayoutSection(group: group)
            section.contentInsets = NSDirectionalEdgeInsets(top: 10, leading: 10, bottom: 10, trailing: 10)
            section.interGroupSpacing = 10
            return section
        }
    }
}

extension MoviesCollectionViewController {
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if dataSource.snapshot().numberOfSections - 1 == indexPath.section {
            let section = dataSource.snapshot().sectionIdentifiers[indexPath.section]
            if dataSource.snapshot().numberOfItems(inSection: section) - 1 == indexPath.row {
                loadMoreMovieItems()
            }
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let item = dataSource.itemIdentifier(for: indexPath) else { return }
        item.select()
    }
}
