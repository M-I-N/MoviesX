//
//  MovieItemCell.swift
//  MoviesX
//
//  Created by Mufakkharul Islam Nayem on 11/5/22.
//

import UIKit
import Kingfisher
import TagListView

class MovieItemCell: UICollectionViewCell {
    
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var releaseDateLabel: UILabel!
    @IBOutlet private weak var voteAverageLabel: UILabel!
    @IBOutlet private weak var voteCountLabel: UILabel!
    @IBOutlet private weak var tagListView: TagListView!
    
    private static let reuseId = "Cell"
    
    var viewModel: MovieItemViewModel? {
        didSet {
            imageView.kf.setImage(with: viewModel?.posterURL)
            titleLabel.text = viewModel?.title
            releaseDateLabel.text = viewModel?.releaseDateFormatted
            voteAverageLabel.attributedText = viewModel?.voteAverageFormatted
            voteCountLabel.text = viewModel?.voteCountFormatted
            tagListView.removeAllTags()
            tagListView.addTags(viewModel?.genreNames ?? [])
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.layer.cornerRadius = 10.0
        contentView.layer.masksToBounds = true
        imageView.layer.cornerRadius = 10.0
    }
    
    static func registerForReuse(with collectionView: UICollectionView) {
        let nib = UINib(nibName: String(describing: self), bundle: Bundle(for: Self.self))
        collectionView.register(nib, forCellWithReuseIdentifier: reuseId)
    }
    
    static func dequeue(fromCollectionView collectionView: UICollectionView, atIndex index: IndexPath) -> MovieItemCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseId, for: index) as? MovieItemCell else {
            fatalError("*** Failed to dequeue \(String(describing: self)) ***")
        }
        return cell
    }

}
