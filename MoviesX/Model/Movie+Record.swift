//
//  Movie+Record.swift
//  MoviesX
//
//  Created by Mufakkharul Islam Nayem on 17/5/22.
//

import Foundation

extension Movie {
    init?(record: MovieRecord) {
        guard let originalLanguage = record.originalLanguage,
              let originalTitle = record.originalTitle,
              let overview = record.overview,
              let title = record.title else { return nil }
        
        adult = record.adult
        backdropPath = record.backdropPath
        genreIDS = record.genreIDS ?? []
        id = Int(record.id)
        self.originalLanguage = originalLanguage
        self.originalTitle = originalTitle
        self.overview = overview
        popularity = record.popularity
        posterPath = record.posterPath
        releaseDate = record.releaseDate
        self.title = title
        video = record.video
        voteAverage = record.voteAverage
        voteCount = Int(record.voteCount)
    }
    
    func asMovieRecord(_ movieRecord: inout MovieRecord) {
        movieRecord.adult = adult
        movieRecord.backdropPath = backdropPath
        movieRecord.genreIDS = genreIDS
        movieRecord.id = Int32(id)
        movieRecord.originalLanguage = originalLanguage
        movieRecord.originalTitle = originalTitle
        movieRecord.overview = overview
        movieRecord.popularity = popularity
        movieRecord.posterPath = posterPath
        movieRecord.releaseDate = releaseDate
        movieRecord.title = title
        movieRecord.video = video
        movieRecord.voteAverage = voteAverage
        movieRecord.voteCount = Int32(voteCount)
    }
    
    var matchingIdPredicate: NSPredicate {
        NSPredicate(format: "id = \(id)")
    }
}
