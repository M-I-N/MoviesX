//
//  Genre+Record.swift
//  MoviesX
//
//  Created by Mufakkharul Islam Nayem on 17/5/22.
//

import Foundation

extension Genre {
    init?(record: GenreRecord) {
        guard let name = record.name else { return nil }
        id = Int(record.id)
        self.name = name
    }
    
    func asMovieRecord(_ genreRecord: inout GenreRecord) {
        genreRecord.id = Int32(id)
        genreRecord.name = name
    }
}
