//
//  DispatchQueue+Extras.swift
//  MoviesX
//
//  Created by Mufakkharul Islam Nayem on 11/5/22.
//

import Foundation

extension DispatchQueue {
    static func mainAsyncIfNeeded(execute work: @escaping () -> Void) {
        if Thread.isMainThread {
            work()
        } else {
            main.async(execute: work)
        }
    }
}
