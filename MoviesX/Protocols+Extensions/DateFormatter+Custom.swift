//
//  DateFormatter+Custom.swift
//  MoviesX
//
//  Created by Mufakkharul Islam Nayem on 17/5/22.
//

import Foundation

typealias CustomDateFormatter = ((String) -> String?)

extension DateFormatter {
    static func convertedDateString(from dateString: String) -> String? {
        let inputDateFormat = "yyyy-MM-ddd"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = inputDateFormat
        
        guard let date = dateFormatter.date(from: dateString) else { return nil }
        let outputDateFormat = "dd MMM yyyy"
        dateFormatter.dateFormat = outputDateFormat
        return dateFormatter.string(from: date)
    }
}
