//
//  ActivityIndicatorLoadable.swift
//  Messenger
//
//  Created by Mufakkharul Islam Nayem on 25/11/20.
//

import UIKit

public final class ObjectAssociation<T: AnyObject> {
    
    private let policy: objc_AssociationPolicy
    
    /// - Parameter policy: An association policy that will be used when linking objects.
    public init(policy: objc_AssociationPolicy = .OBJC_ASSOCIATION_RETAIN_NONATOMIC) {
        self.policy = policy
    }
    
    /// Accesses associated object.
    /// - Parameter index: An object whose associated object is to be accessed.
    public subscript(index: AnyObject) -> T? {
        get { return objc_getAssociatedObject(index, Unmanaged.passUnretained(self).toOpaque()) as? T }
        set { objc_setAssociatedObject(index, Unmanaged.passUnretained(self).toOpaque(), newValue, policy) }
    }
    
}

extension UIViewController {
    private static let association = ObjectAssociation<UIActivityIndicatorView>()
    
    var activityIndicatorView: UIActivityIndicatorView {
        get {
            if let indicator = UIViewController.association[self] {
                return indicator
            } else {
                let activityIndicatorView: UIActivityIndicatorView = UIActivityIndicatorView(style: .medium)
                UIViewController.association[self] = activityIndicatorView
                return UIViewController.association[self]!
            }
        }
        set { UIViewController.association[self] = newValue }
    }
    
    @objc fileprivate func addIndicatorToView() {
        self.view.addSubview(self.activityIndicatorView)
        
        self.activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            self.activityIndicatorView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            self.activityIndicatorView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor)
        ])
        
        self.view.bringSubviewToFront(self.activityIndicatorView)
    }
    
    @objc func startLoadingAnimation() {
        DispatchQueue.main.async {
            self.addIndicatorToView()
            self.activityIndicatorView.startAnimating()
            // UIApplication.shared.beginIgnoringInteractionEvents() // if desired
        }
    }

    @objc func stopLoadingAnimation() {
        DispatchQueue.main.async {
            self.activityIndicatorView.stopAnimating()
            // UIApplication.shared.endIgnoringInteractionEvents()
        }
    }
}

extension UITableViewController {
    override func addIndicatorToView() {
        self.tableView.backgroundView = activityIndicatorView
    }
}

extension UICollectionViewController {
    override func addIndicatorToView() {
        self.collectionView.backgroundView = activityIndicatorView
    }
}
