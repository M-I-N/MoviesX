//
//  GenreStore.swift
//  MoviesX
//
//  Created by Mufakkharul Islam Nayem on 17/5/22.
//

import Foundation

class GenreStore: PersistentStore {
    /**
     * `Singleton` design pattern is welcomed but also keeping the initialization process wide open
     * which may help testing easier
     * This pattern is found with `URLSession` object
     */
    static let shared = GenreStore()
    
    private(set) var genres = [Genre]()
    
    func loadGenreList(completion: @escaping (Result<[Genre], Error>) -> Void) {
        if genres.isEmpty {
            do {
                let genreRecords = try fecthRecords(of: GenreRecord.self)
                self.genres = genreRecords.compactMap(Genre.init)
                completion(.success(self.genres))
            } catch {
                completion(.failure(error))
            }
        } else {
            completion(.success(genres))
        }
    }
    
    func save(_ newGenres: [Genre]) {
        for newGenre in newGenres {
            var genreRecord = initManagedObject(of: GenreRecord.self)
            newGenre.asMovieRecord(&genreRecord)
        }
        do {
            try saveContext()
            self.genres = newGenres
        } catch {
            print(error)
        }
    }
}
