//
//  FavoritesStore.swift
//  MoviesX
//
//  Created by Mufakkharul Islam Nayem on 16/5/22.
//

import Foundation

enum FavoritesStoreError: Error {
    case alreadyAddedInStore
}

class FavoritesStore: PersistentStore {
    /**
     * `Singleton` design pattern is welcomed but also keeping the initialization process wide open
     * which may help testing easier
     * This pattern is found with `URLSession` object
     */
    static let shared = FavoritesStore()
    
    func isFavorite(movie: Movie) -> Bool {
        let context = persistentContainer.viewContext
        let request = MovieRecord.fetchRequest()
        request.predicate = movie.matchingIdPredicate
        do {
            let count = try context.count(for: request)
            return count > 0
        } catch {
            return false
        }
    }
    
    func loadMovies(completion: @escaping (Result<[Movie], Error>) -> Void) {
        do {
            let movieRecords = try fecthRecords(of: MovieRecord.self)
            let movies = movieRecords.compactMap(Movie.init)
            completion(.success(movies))
        } catch {
            completion(.failure(error))
        }
    }
    
    func add(movie: Movie) throws {
        guard !isFavorite(movie: movie) else { throw FavoritesStoreError.alreadyAddedInStore }
        var movieRecord = initManagedObject(of: MovieRecord.self)
        movie.asMovieRecord(&movieRecord)
        try saveContext()
    }
    
    func remove(movie: Movie) throws {
        let filter = movie.matchingIdPredicate
        let records = try fecthRecords(of: MovieRecord.self, filter: filter)
        let context = persistentContainer.viewContext
        for record in records {
            context.delete(record)
        }
        try saveContext()
    }
}
