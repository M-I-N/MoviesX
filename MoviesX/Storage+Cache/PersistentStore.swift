//
//  PersistentStore.swift
//  MoviesX
//
//  Created by Mufakkharul Islam Nayem on 17/5/22.
//

import CoreData

/// Provide a set of common functionalities related to Core Data. For actual usage, look for the specialized subclasses.
class PersistentStore {
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "MoviesX")
        container.loadPersistentStores { _, error in
            if let error = error {
                fatalError(error.localizedDescription)
            }
        }
        container.viewContext.mergePolicy = NSMergeByPropertyStoreTrumpMergePolicy
        container.viewContext.automaticallyMergesChangesFromParent = true
        return container
    }()
    
    func saveContext() throws {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            try context.save()
        }
    }
    
    func initManagedObject<T: NSManagedObject>(of type: T.Type) -> T {
        let managedObject = T(context: persistentContainer.viewContext)
        return managedObject
    }
    
    func fecthRecords<T: NSManagedObject>(of type: T.Type, filter predicate: NSPredicate? = nil) throws -> [T] {
        let context = persistentContainer.viewContext
        let entityName = String(describing: T.self)
        let request = NSFetchRequest<T>(entityName: entityName)
        request.predicate = predicate
        let records = try context.fetch(request)
        return records
    }
}
