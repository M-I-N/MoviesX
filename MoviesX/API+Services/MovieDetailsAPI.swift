//
//  MovieDetailsAPI.swift
//  MoviesX
//
//  Created by Mufakkharul Islam Nayem on 15/5/22.
//

import Foundation
import Combine

class MovieDetailsAPI: WebService {
    
    private init() { }
    
    static let shared = MovieDetailsAPI()
    
    lazy var session: URLSession = {
        let config = URLSessionConfiguration.default
        config.allowsExpensiveNetworkAccess = false
        config.allowsConstrainedNetworkAccess = false
        config.waitsForConnectivity = true
        return URLSession(configuration: config)
    }()
    
    private var cancellables = Set<AnyCancellable>()
    
    deinit {
        session.finishTasksAndInvalidate()
    }
    
    func getMovieDetails(forMovieId movieId: Int, completion: @escaping (Result<MovieDetails, Error>) -> Void) {
        let request = MovieDetailsEndpoint(movieId: movieId).request
        
        publisher(for: request)
            .receive(on: DispatchQueue.main)
            .sink { receiveCompletion in
                switch receiveCompletion {
                case .failure(let error):
                    completion(.failure(error))
                default:
                    break
                }
            } receiveValue: { (movieDetails: MovieDetails) in
                completion(.success(movieDetails))
            }
            .store(in: &cancellables)

    }
}

struct MovieDetailsEndpoint: Endpoint {
    let movieId: Int
    
    var base: String { MovieEndpointDefaults.domain.rawValue }
    var path: String { "/3/movie/\(movieId)" }
    var queryItems: [URLQueryItem]? {
        [.init(name: MovieEndpointKeys.apiKey.rawValue, value: MovieEndpointDefaults.apiKey.rawValue)]
    }
}
