//
//  MoviesAPI.swift
//  MoviesX
//
//  Created by Mufakkharul Islam Nayem on 11/5/22.
//

import Foundation
import Combine

class MoviesAPI: WebService {
    private init() { }
    
    static let shared = MoviesAPI()
    
    lazy var session: URLSession = {
        let config = URLSessionConfiguration.default
        config.allowsExpensiveNetworkAccess = false
        config.allowsConstrainedNetworkAccess = false
        config.waitsForConnectivity = true
        return URLSession(configuration: config)
    }()
    
    private var cancellables = Set<AnyCancellable>()
    
    deinit {
        session.finishTasksAndInvalidate()
    }
    
    func getMovies(for movieType: MovieType, page: Int, completion: @escaping (Result<[Movie], Error>) -> Void) {
        
        publisher(for: movieType.buildRequestFor(page: page))
            .receive(on: DispatchQueue.main)
            .sink { receiveCompletion in
                switch receiveCompletion {
                case .failure(let error):
                    completion(.failure(error))
                default:
                    break
                }
            } receiveValue: { (movies: Movies) in
                completion(.success(movies.results))
            }
            .store(in: &cancellables)
        
    }
}

enum MovieType {
    case nowPlaying
    case popular
    case topRated
    case upcoming
    
    func buildRequestFor(page: Int) -> URLRequest {
        switch self {
        case .nowPlaying:
            return NowPlayingMoviesEndpoint(page: page).request
        case .popular:
            return PopularMoviesEndpoint(page: page).request
        case .topRated:
            return TopRatedMoviesEndpoint(page: page).request
        case .upcoming:
            return UpcomingMoviesEndpoint(page: page).request
        }
    }

    struct NowPlayingMoviesEndpoint: Endpoint {
        var base: String { MovieEndpointDefaults.domain.rawValue }
        let path: String = "/3/movie/now_playing"
        let page: Int
        var queryItems: [URLQueryItem]? {
            [.init(name: MovieEndpointKeys.page.rawValue, value: "\(page)"),
             .init(name: MovieEndpointKeys.apiKey.rawValue, value: MovieEndpointDefaults.apiKey.rawValue)]
        }
    }

    struct PopularMoviesEndpoint: Endpoint {
        var base: String { MovieEndpointDefaults.domain.rawValue }
        let path: String = "/3/movie/popular"
        let page: Int
        var queryItems: [URLQueryItem]? {
            [.init(name: MovieEndpointKeys.page.rawValue, value: "\(page)"),
             .init(name: MovieEndpointKeys.apiKey.rawValue, value: MovieEndpointDefaults.apiKey.rawValue)]
        }
    }

    struct TopRatedMoviesEndpoint: Endpoint {
        var base: String { MovieEndpointDefaults.domain.rawValue }
        let path: String = "/3/movie/top_rated"
        let page: Int
        var queryItems: [URLQueryItem]? {
            [.init(name: MovieEndpointKeys.page.rawValue, value: "\(page)"),
             .init(name: MovieEndpointKeys.apiKey.rawValue, value: MovieEndpointDefaults.apiKey.rawValue)]
        }
    }

    struct UpcomingMoviesEndpoint: Endpoint {
        var base: String { MovieEndpointDefaults.domain.rawValue }
        let path: String = "/3/movie/upcoming"
        let page: Int
        var queryItems: [URLQueryItem]? {
            [.init(name: MovieEndpointKeys.page.rawValue, value: "\(page)"),
             .init(name: MovieEndpointKeys.apiKey.rawValue, value: MovieEndpointDefaults.apiKey.rawValue)]
        }
    }

}
