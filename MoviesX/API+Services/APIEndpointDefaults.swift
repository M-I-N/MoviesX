//
//  APIEndpointDefaults.swift
//  MoviesX
//
//  Created by Mufakkharul Islam Nayem on 15/5/22.
//

import Foundation

enum MovieEndpointKeys: String {
    case apiKey = "api_key"
    case page
    case query
}

enum MovieEndpointDefaults: String {
    case domain = "https://api.themoviedb.org"
    case imagePathDomain = "https://image.tmdb.org/t/p/original/"
    case apiKey = "0e7274f05c36db12cbe71d9ab0393d47"
}
