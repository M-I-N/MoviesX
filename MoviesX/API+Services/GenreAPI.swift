//
//  GenreAPI.swift
//  MoviesX
//
//  Created by Mufakkharul Islam Nayem on 17/5/22.
//

import Foundation
import Combine

class GenreAPI: WebService {
    private init() { }
    
    static let shared = GenreAPI()
    
    lazy var session: URLSession = {
        let config = URLSessionConfiguration.default
        config.allowsExpensiveNetworkAccess = false
        config.allowsConstrainedNetworkAccess = false
        config.waitsForConnectivity = true
        return URLSession(configuration: config)
    }()
    
    private var cancellables = Set<AnyCancellable>()
    
    deinit {
        session.finishTasksAndInvalidate()
    }
    
    func getList(completion: @escaping (Result<[Genre], Error>) -> Void) {
        let request = GenreEndpoint().request
        
        publisher(for: request)
            .receive(on: DispatchQueue.main)
            .sink { receiveCompletion in
                switch receiveCompletion {
                case .failure(let error):
                    completion(.failure(error))
                default:
                    break
                }
            } receiveValue: { (genreResponse: GenreResponse) in
                completion(.success(genreResponse.genres))
            }
            .store(in: &cancellables)
    }
}

struct GenreEndpoint: Endpoint {
    var base: String { MovieEndpointDefaults.domain.rawValue }
    let path: String = "/3/genre/movie/list"
    var queryItems: [URLQueryItem]? {
        [.init(name: MovieEndpointKeys.apiKey.rawValue, value: MovieEndpointDefaults.apiKey.rawValue)]
    }
}
