//
//  Service+Adapter.swift
//  MoviesX
//
//  Created by Mufakkharul Islam Nayem on 11/5/22.
//

import Foundation

class MoviesAPIItemServiceAdapter: ItemService {
    let api: MoviesAPI
    let genreStore: GenreStore
    let movieType: MovieType
    let select: (Movie) -> Void
    let dateFormattingClosure: CustomDateFormatter?
    private var page = 1
    
    init(api: MoviesAPI,
         genreStore: GenreStore,
         movieType: MovieType,
         selection: @escaping (Movie) -> Void,
         dateFormattingClosure: CustomDateFormatter?) {
        self.api = api
        self.genreStore = genreStore
        self.movieType = movieType
        self.select = selection
        self.dateFormattingClosure = dateFormattingClosure
    }
}

extension MoviesAPIItemServiceAdapter {
    func loadFirstPageItems(completion: @escaping (Result<[MovieItemViewModel], Error>) -> Void) {
        let genres = genreStore.genres
        api.getMovies(for: movieType, page: 1) { [weak self] result in
            DispatchQueue.mainAsyncIfNeeded {
                switch result {
                case .success(let movies):
                    let movieItems = movies.map { movie in
                        MovieItemViewModel(
                            movie: movie,
                            genres: genres,
                            selection: {
                                self?.select(movie)
                            },
                            dateFormattingClosure: self?.dateFormattingClosure
                        )
                    }
                    
                    completion(.success(movieItems))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
    
    func loadNextPageItems(completion: @escaping (Result<[MovieItemViewModel], Error>) -> Void) {
        let genres = genreStore.genres
        api.getMovies(for: movieType, page: page + 1) { [weak self] result in
            DispatchQueue.mainAsyncIfNeeded {
                switch result {
                case .success(let movies):
                    let movieItems = movies.map { movie in
                        MovieItemViewModel(
                            movie: movie,
                            genres: genres,
                            selection: {
                                self?.select(movie)
                            },
                            dateFormattingClosure: self?.dateFormattingClosure
                        )
                    }
                    
                    completion(.success(movieItems))
                    // increment page number for next successive calls
                    self?.page += 1
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
}

struct MovieDetailsAPIItemServiceAdapter: MovieDetailsItemService {
    let movie: Movie
    let api: MovieDetailsAPI
    let store: FavoritesStore
    let dateFormattingClosure: CustomDateFormatter?
    
    func loadMovieDetails(completion: @escaping MovieDetailViewModel.NewMovieDetailClosure) {
        api.getMovieDetails(forMovieId: movie.id) { result in
            DispatchQueue.mainAsyncIfNeeded {
                switch result {
                case .success(let details):
                    let isFavorite = store.isFavorite(movie: movie)
                    let detailItem = MovieDetailViewModel(
                        movieDetails: details,
                        isFavorite: isFavorite,
                        addToFavorite: { addCompletion in
                            addMovieToFavorites(movieDetails: details, completion: addCompletion)
                        },
                        removeFromFavorite: { removeCompletion in
                            removeMovieFromFavorites(movieDetails: details, completion: removeCompletion)
                        },
                        dateFormattingClosure: dateFormattingClosure
                    )
                    
                    completion(.success(detailItem))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
    
    func addMovieToFavorites(movieDetails: MovieDetails, completion: @escaping MovieDetailViewModel.NewMovieDetailClosure) {
        do {
            try store.add(movie: movie)
            let new = MovieDetailViewModel(
                movieDetails: movieDetails,
                isFavorite: true,
                addToFavorite: { addCompletion in
                    addMovieToFavorites(movieDetails: movieDetails, completion: addCompletion)
                },
                removeFromFavorite: { removeCompletion in
                    removeMovieFromFavorites(movieDetails: movieDetails, completion: removeCompletion)
                },
                dateFormattingClosure: dateFormattingClosure
            )
            
            completion(.success(new))
        } catch {
            completion(.failure(error))
        }
    }
    
    func removeMovieFromFavorites(movieDetails: MovieDetails, completion: @escaping MovieDetailViewModel.NewMovieDetailClosure) {
        do {
            try store.remove(movie: movie)
            let new = MovieDetailViewModel(
                movieDetails: movieDetails,
                isFavorite: false,
                addToFavorite: { addCompletion in
                    addMovieToFavorites(movieDetails: movieDetails, completion: addCompletion)
                },
                removeFromFavorite: { removeCompletion in
                    removeMovieFromFavorites(movieDetails: movieDetails, completion: removeCompletion)
                },
                dateFormattingClosure: dateFormattingClosure
            )
            
            completion(.success(new))
        } catch {
            completion(.failure(error))
        }
    }
}

class MovieSearchAPIItemServiceAdapter: ItemService {
    let api: MovieSearchAPI
    let genreStore: GenreStore
    var query: String?
    let select: (Movie) -> Void
    let dateFormattingClosure: CustomDateFormatter?
    private var page = 1
    
    init(api: MovieSearchAPI,
         genreStore: GenreStore,
         selection: @escaping (Movie) -> Void,
         dateFormattingClosure: CustomDateFormatter?) {
        self.api = api
        self.genreStore = genreStore
        self.select = selection
        self.dateFormattingClosure = dateFormattingClosure
    }
}

extension MovieSearchAPIItemServiceAdapter {
    func loadFirstPageItems(completion: @escaping (Result<[MovieItemViewModel], Error>) -> Void) {
        guard let query = query else { return completion(.success([])) }    // typically this else should not be executed in reality
        let genres = genreStore.genres
        api.getMovies(forSearch: query, page: 1) { [weak self] result in
            DispatchQueue.mainAsyncIfNeeded {
                switch result {
                case .success(let movies):
                    let movieItems = movies.map { movie in
                        MovieItemViewModel(
                            movie: movie,
                            genres: genres,
                            selection: {
                                self?.select(movie)
                            },
                            dateFormattingClosure: self?.dateFormattingClosure
                        )
                    }
                    
                    completion(.success(movieItems))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
    
    func loadNextPageItems(completion: @escaping (Result<[MovieItemViewModel], Error>) -> Void) {
        guard let query = query else { return completion(.success([])) }    // typically this else should not be executed in reality
        let genres = genreStore.genres
        api.getMovies(forSearch: query, page: page + 1) { [weak self] result in
            DispatchQueue.mainAsyncIfNeeded {
                switch result {
                case .success(let movies):
                    let movieItems = movies.map { movie in
                        MovieItemViewModel(
                            movie: movie,
                            genres: genres,
                            selection: {
                                self?.select(movie)
                            },
                            dateFormattingClosure: self?.dateFormattingClosure
                        )
                    }
                    
                    completion(.success(movieItems))
                    // increment page number for next successive calls
                    self?.page += 1
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
}

struct FavoritesStoreItemServiceAdapter: ItemService {
    let store: FavoritesStore
    let genreStore: GenreStore
    let select: (Movie) -> Void
    let dateFormattingClosure: CustomDateFormatter?
    
    func loadFirstPageItems(completion: @escaping (Result<[MovieItemViewModel], Error>) -> Void) {
        // instant loading is discouraged because UIKit view controller struggles to refresh UI immediately
        let genres = genreStore.genres
        DispatchQueue.global().async {
            store.loadMovies { result in
                DispatchQueue.mainAsyncIfNeeded {
                    switch result {
                    case .success(let movies):
                        let movieItems = movies.map { movie in
                            MovieItemViewModel(
                                movie: movie,
                                genres: genres,
                                selection: {
                                    self.select(movie)
                                },
                                dateFormattingClosure: dateFormattingClosure
                            )
                        }
                        
                        completion(.success(movieItems))
                    case .failure(let error):
                        completion(.failure(error))
                    }
                }
            }
        }
    }
    
    func loadNextPageItems(completion: @escaping (Result<[MovieItemViewModel], Error>) -> Void) {
        // if in the future we consider loading data from cache chunk by chunk, we will implement this
    }
}

/// This class is responsible for genre list fetching and caching into local storage
class GenreAPIServiceAdapter {
    let api: GenreAPI
    let store: GenreStore
    
    init(api: GenreAPI, store: GenreStore) {
        self.api = api
        self.store = store
    }
    
    func preheatGenreList() {
        let apiFetch = { [weak self] in
            self?.api.getList { [weak self] result in
                switch result {
                case .success(let genres):
                    self?.store.save(genres)
                case .failure(let error):
                    print(error)
                }
            }
        }
        store.loadGenreList { result in
            switch result {
            case .success(let genres):
                if genres.isEmpty {
                    apiFetch()
                } else {
                    break
                }
            case .failure(let error):
                // typically this should not be an execution path
                print(error)
                apiFetch()
            }
        }
    }
}
