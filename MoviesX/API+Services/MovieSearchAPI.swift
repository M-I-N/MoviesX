//
//  MovieSearchAPI.swift
//  MoviesX
//
//  Created by Mufakkharul Islam Nayem on 16/5/22.
//

import Foundation
import Combine

class MovieSearchAPI: WebService {
    private init() { }
    
    static let shared = MovieSearchAPI()
    
    lazy var session: URLSession = {
        let config = URLSessionConfiguration.default
        config.allowsExpensiveNetworkAccess = false
        config.allowsConstrainedNetworkAccess = false
        config.waitsForConnectivity = true
        return URLSession(configuration: config)
    }()
    
    private var cancellables = Set<AnyCancellable>()
    
    deinit {
        session.finishTasksAndInvalidate()
    }
    
    func getMovies(forSearch query: String, page: Int, completion: @escaping (Result<[Movie], Error>) -> Void) {
        let request = MovieSearchEndpoint(query: query, page: page).request
        
        publisher(for: request)
            .receive(on: DispatchQueue.main)
            .sink { receiveCompletion in
                switch receiveCompletion {
                case .failure(let error):
                    completion(.failure(error))
                default:
                    break
                }
            } receiveValue: { (movies: Movies) in
                completion(.success(movies.results))
            }
            .store(in: &cancellables)
        
    }
}

struct MovieSearchEndpoint: Endpoint {
    let query: String
    let page: Int
    
    var base: String { MovieEndpointDefaults.domain.rawValue }
    let path: String = "/3/search/movie"
    var queryItems: [URLQueryItem]? {
        [.init(name: MovieEndpointKeys.query.rawValue, value: query),
         .init(name: MovieEndpointKeys.page.rawValue, value: "\(page)"),
         .init(name: MovieEndpointKeys.apiKey.rawValue, value: MovieEndpointDefaults.apiKey.rawValue)]
    }
}
